<?php
/**
 * Created by PhpStorm.
 * User: STS
 * Date: 07/08/2020
 * Time: 13:55
 */
declare(strict_types=1);

namespace MauticPlugin\MauticYetiforceBundle;

use Mautic\IntegrationsBundle\Bundle\AbstractPluginBundle;

class MauticYetiforceBundle extends AbstractPluginBundle
{
}