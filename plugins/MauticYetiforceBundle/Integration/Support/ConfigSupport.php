<?php
/**
 * Created by PhpStorm.
 * User: STS
 * Date: 07/08/2020
 * Time: 14:17
 */
namespace MauticPlugin\MauticYetiforce\Integration\Support;
use Mautic\IntegrationsBundle\Integration\Interfaces\ConfigFormAuthInterface;
use Mautic\IntegrationsBundle\Integration\Interfaces\ConfigFormInterface;
use MauticPlugin\HelloWorldBundle\Form\Type\ConfigAuthType;
use MauticPlugin\MauticYetiforceBundle\Integration\MauticYetiforceIntegration;

class ConfigSupport  extends MauticYetiforceIntegration implements ConfigFormInterface, ConfigFormAuthInterface
{

    /**
     * Return the name/class of the form type to override the default or just return NULL to use the default.
     *
     * @return string|null Name of the form type service
     */
    public function getConfigFormName(): ?string
    {
        // TODO: Implement getConfigFormName() method.
    }

    /**
     * Return the template to use from the controller. Return null to use the default.
     *
     * @return string|null Name of the template like SomethingBundle:Config:form.html.php
     */
    public function getConfigFormContentTemplate(): ?string
    {
        // TODO: Implement getConfigFormContentTemplate() method.
    }

    /**
     * Return the name of the form type service for the authorization tab which should include all the fields required for the API to work.
     */
    public function getAuthConfigFormName(): string
    {
        return ConfigAuthType::class;
    }
}