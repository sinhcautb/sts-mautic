<?php

declare(strict_types=1);

return [
    'name' => 'Mautic Yetifore',
    'description' => 'This is plugin Mautic mapping database in Yetifore',
    'version' => '1.0.1',
    'author' => 'STS GROUP',
    'services' => [
        'forms'  => array(
            'plugin.mauticyetifore.form' => array(
                'class' => 'MauticPlugin\MauticYetiforceBundle\Form\Type\MauticYetiforceType',
                'alias' => 'mauticyetifore'
            )
        ),
//        'other'   => array(
//            'plugin.mauticyetifore.token.validator' => array(
//                'class'     => 'MauticPlugin\MauticYetiforceBundle\Form\Validator\Constraints\TokenValidator',
//                'arguments' => 'mautic.factory',
//                'tag'       => 'validator.constraint_validator',
//                'alias'     => 'mauticyetifore_token'
//            )
//        ),
        'integrations' => [
            // Basic definitions with name, display name and icon
            'mautic.integration.mauticyetifore' => [
                'class' => \MauticPlugin\MauticYetiforceBundle\Integration\MauticYetiforceIntegration::class,
                'tags'  => [
                    'mautic.integration',
                    'mautic.basic_integration',
                ],
            ],
            // Provides the form types to use for the configuration UI
            'mautic.integration.mauticyetifore.configuration' => [
                'class'     => \MauticPlugin\MauticYetiforceBundle\Integration\Support\ConfigSupport::class,
                'tags'      => [
                    'mautic.config_integration',
                ],
            ],
        ],
        // ...
    ],
    // ...
];
